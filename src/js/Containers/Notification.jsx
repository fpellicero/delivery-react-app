import React from 'react';
import {connect} from 'react-redux';
import HideNotification from '../actions/HideNotification';
import NotificationComponent from '../Components/NotificationComponent';

const mapStoreToProps = (store) => (
  {
    text: store.Notification
  }
);

const mapDispatchToProps = (dispatch) => (
  {
    HideNotification: () => {dispatch(HideNotification())}
  }
)

export default connect(mapStoreToProps, mapDispatchToProps)(NotificationComponent);