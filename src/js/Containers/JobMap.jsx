import React from 'react';
import {connect} from 'react-redux';
import JobMapComponent from '../Components/JobMapComponent';

const mapStoreToProps = (store) => (
  {
    PickupAddress: store.PickupAddress,
    DropoffAddress: store.DropoffAddress
  }
);

export default connect(mapStoreToProps)(JobMapComponent);