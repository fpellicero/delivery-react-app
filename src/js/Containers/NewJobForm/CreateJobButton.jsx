import {connect} from 'react-redux';
import * as ApiManager from '../../Modules/ApiManager';
import ResetFormAction from '../../actions/ResetForm';
import ShowNotification from '../../actions/ShowNotification';
import SetRequestPending from '../../actions/SetRequestPending';
import CreateJobButtonComponent from '../../Components/NewJobForm/CreateJobButtonComponent';

const CreateJob = (PickupAddress, DropoffAddress, dispatch) => {
  const Success = () => {
    dispatch(ResetFormAction());
    dispatch(ShowNotification('Job created successfully!'));
  };

  const ShowError = () => {
    dispatch(ShowNotification('Error creating job, please try again'));
    dispatch(SetRequestPending(false));
  };

  dispatch(SetRequestPending(true));
  ApiManager.CreateJob(PickupAddress, DropoffAddress)
    .then((response) => {
      const isOk = !response.code || response.code != 'JOB_ERROR';
      
      isOk ? Success() : ShowError();
    })
    .catch((response) => {
      dispatch(ShowNotification('Error creating job, please try again'))
    })
}

const mapStoreToProps = (store) => (
  {
    PickupAddress: store.PickupAddress,
    DropoffAddress: store.DropoffAddress,
    RequestPending: store.RequestPending
  }
);

const mapDispatchToProps = (dispatch) => (
  {
    CreateJob: (PickupAddress, DropoffAddress) => CreateJob(PickupAddress, DropoffAddress, dispatch)
  }
);

export default connect(mapStoreToProps, mapDispatchToProps)(CreateJobButtonComponent)