import {connect} from 'react-redux';
import SetPickupAddress from '../../actions/SetPickupAddress';
import AddressInputComponent from '../../Components/NewJobForm/AddressInput';
import Icons from '../../Modules/Icons';
import {Geocode} from '../../Modules/ApiManager';

const BadgesUrl = {
  EMPTY: Icons.PICKUP_EMPTY_FIELD,
  OK: Icons.PICKUP_OK_FIELD,
  ERROR: Icons.PICKUP_ERROR_FIELD
}

const GeocodeAndSetAddress = (address, dispatch) => {
  const setAddress = (value) => dispatch(SetPickupAddress(value));
  
  address 
    ? Geocode(address).then(setAddress)
    : setAddress('');
}

const mapStoreToProps = (store) => (
  {
    placeholder: 'Pick up address',
    Address: store.PickupAddress,
    BadgesUrl: BadgesUrl
  }
);

const mapDispatchToProps = (dispatch) => (
  {
    SetAddress: (value) => GeocodeAndSetAddress(value, dispatch)
  }
);

export default connect(mapStoreToProps, mapDispatchToProps)(AddressInputComponent)