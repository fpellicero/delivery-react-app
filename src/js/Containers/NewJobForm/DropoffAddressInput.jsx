import {connect} from 'react-redux';
import SetDropoffAddress from '../../actions/SetDropoffAddress';
import AddressInputComponent from '../../Components/NewJobForm/AddressInput';
import Icons from '../../Modules/Icons';
import {Geocode} from '../../Modules/ApiManager';

const BadgesUrl = {
  EMPTY: Icons.DROPOFF_EMPTY_FIELD,
  OK: Icons.DROPOFF_OK_FIELD,
  ERROR: Icons.DROPOFF_ERROR_FIELD
}

const GeocodeAndSetAddress = (address, dispatch) => {
  const setAddress = (value) => dispatch(SetDropoffAddress(value));
  
  address 
    ? Geocode(address).then(setAddress)
    : setAddress('');
}

const mapStoreToProps = (store) => (
  {
    placeholder: 'Drop off address',
    Address: store.DropoffAddress,
    BadgesUrl: BadgesUrl
  }
);

const mapDispatchToProps = (dispatch) => (
  {
    SetAddress: (value) => GeocodeAndSetAddress(value, dispatch)
  }
);

export default connect(mapStoreToProps, mapDispatchToProps)(AddressInputComponent)