import React from 'react';
import AppComponent from '../Components/AppComponent';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducers from '../reducers';

const App = () => (
  <Provider store={createStore(reducers)}>
    <AppComponent />
  </Provider>
);

export default App;