import React from 'react';
import PropTypes from 'prop-types';
import {withGoogleMap, withScriptjs, GoogleMap, Marker} from 'react-google-maps';
import Icons from '../Modules/Icons';

const PontAlexandreIII = {lat: 48.863879, lng: 2.312670};
const CustomMap = withScriptjs(withGoogleMap((props) => (
  <GoogleMap
    defaultCenter={PontAlexandreIII}
    defaultZoom={15}
  >
    {props.children}
  </GoogleMap>
)));

const GetMarker = (address, icon) => (
  <Marker position={{lat: address.latitude, lng: address.longitude}} icon = {{url: icon}} animation={4}/>
)

const fullSizeEmptyDiv = <div style={{ height: `100%` }} />;
const JobMap = ({PickupAddress, DropoffAddress}) => 
(
  <CustomMap
    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp"
    loadingElement={fullSizeEmptyDiv}
    containerElement={fullSizeEmptyDiv}
    mapElement={fullSizeEmptyDiv} >
      {PickupAddress.Valid && GetMarker(PickupAddress.Value, Icons.PICKUP_MARKER)}
      {DropoffAddress.Valid && GetMarker(DropoffAddress.Value, Icons.DROPOFF_MARKER)}
  </CustomMap>
);

JobMap.propTypes = {
  PickupAddress: PropTypes.object.isRequired,
  DropoffAddress: PropTypes.object.isRequired
}

export default JobMap;