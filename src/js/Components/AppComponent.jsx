import React from 'react';
import JobMap from '../Containers/JobMap';
import NewJobForm from './NewJobForm/NewJobForm';
import Notification from '../Containers/Notification';
import '../../scss/App.scss';

const AppComponent = () => (
  <div id='app-container'>
    <NewJobForm />
    <JobMap />
    <Notification />
  </div>
)

export default AppComponent;