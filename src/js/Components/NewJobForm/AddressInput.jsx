import React, {Component} from 'react';
import PropTypes from 'prop-types';
import FormRow from './FormRow';

const GEOCODE_DELAY_AFTER_STOP_TYPING = 1000;
export default class AddressInput extends Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate(prevProps) {
    const {Address} = this.props;
    if(Address.Valid) {
      this.input.value = Address.Value.address;
    }
    if(!Address.Filled) {
      this.input.value = '';
    }
  }

  GetBadge() {
    const {Address, BadgesUrl} = this.props;
    if(!Address.Filled) return BadgesUrl.EMPTY;
    
    return Address.Valid 
      ? BadgesUrl.OK
      : BadgesUrl.ERROR;
  }

  onKeyUp() {
    const {SetAddress} = this.props;

    clearTimeout(this.geocodeDebouncer);
    this.geocodeDebouncer = setTimeout(() => {
      SetAddress(this.input.value);
    }, GEOCODE_DELAY_AFTER_STOP_TYPING);
  }

  SubmitValue() {
    const {SetAddress} = this.props;

    clearTimeout(this.geocodeDebouncer);
    SetAddress(this.input.value);
  }

  GetInputProps() {
    const {SetAddress, placeholder} = this.props;
    return {
      type: "text",
      placeholder: placeholder,
      onBlur: (e) => {SetAddress(e.target.value)},
      onKeyUp: (e) => {this.onKeyUp()},
      ref: (element) => {this.input = element}
    }
  }

  render() {
    return (
      <FormRow
        badgeElement={<img src={this.GetBadge()}/>}
        inputElement={<input {...this.GetInputProps()}  />} />
    );
  }
}

AddressInput.defaultProps = {
  placeholder: ''
}

AddressInput.propTypes = {
  Address: PropTypes.object.isRequired,
  placeholder: PropTypes.string,
  BadgesUrl: PropTypes.object.isRequired,
  SetAddress: PropTypes.func.isRequired
}
