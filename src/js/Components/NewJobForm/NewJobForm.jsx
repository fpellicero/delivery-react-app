import React from 'react';
import PickupAddressInput from '../../Containers/NewJobForm/PickupAddressInput';
import DropoffAddressInput from '../../Containers/NewJobForm/DropoffAddressInput';
import CreateJobButton from '../../Containers/NewJobForm/CreateJobButton';
import '../../../scss/NewJobForm/NewJobForm.scss';

const NewJobForm = () => (
  <div id="new-job-form">
    <PickupAddressInput />

    <DropoffAddressInput />

    <CreateJobButton />
  </div>
);

export default NewJobForm;