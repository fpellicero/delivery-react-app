import React from 'react';
import PropTypes from 'prop-types';
import FormRow from './FormRow';
import '../../../scss/NewJobForm/CreateJobButton.scss';

const IsEnabled = (PickupAddress, DropoffAddress, RequestPending) => PickupAddress.Valid && DropoffAddress.Valid && !RequestPending;
const GetClassNames = (PickupAddress, DropoffAddress, RequestPending) => `createJobButton${(IsEnabled(PickupAddress, DropoffAddress, RequestPending) ? "" : " disabled")}`;
const GetButtonText = (RequestPending) => RequestPending ? "Creating..." : "Create job"

const GetButton = (PickupAddress, DropoffAddress, CreateJob, RequestPending) => (
  <button 
    className={GetClassNames(PickupAddress, DropoffAddress, RequestPending)}
    onClick={() => {CreateJob(PickupAddress, DropoffAddress)}}>
    {GetButtonText(RequestPending)}
  </button>
)

const CreateJobButtonComponent = ({PickupAddress, DropoffAddress, CreateJob, RequestPending}) => (
  <FormRow 
    inputElement={GetButton(PickupAddress, DropoffAddress, CreateJob, RequestPending)}
  />
)

CreateJobButtonComponent.propTypes = {
  PickupAddress: PropTypes.object.isRequired,
  DropoffAddress: PropTypes.object.isRequired,
  CreateJob: PropTypes.func.isRequired,
  RequestPending: PropTypes.bool.isRequired
}

export default CreateJobButtonComponent

