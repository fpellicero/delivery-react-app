import React from 'react';
import PropTypes from 'prop-types';
import '../../../scss/NewJobForm/FormRow.scss';

const FormRow = ({badgeElement, inputElement}) => (
  <div className="row">
    <div className="formBadge">
      {badgeElement}
    </div>
    {inputElement}
  </div>
)

FormRow.defaultProps = {
  badgeElement: null
}

FormRow.propTypes = {
  badgeElement: PropTypes.element,
  inputElement: PropTypes.element.isRequired
}

export default FormRow;