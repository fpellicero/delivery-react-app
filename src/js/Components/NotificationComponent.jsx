import React from 'react';
import '../../scss/Notification.scss';

export default class Notification extends React.Component {
  constructor(props) {
    super(props);
  }
  
  componentDidUpdate(prevProps) {
    if(!prevProps.text && this.props.text) {
      this.hideTimer = setTimeout(() => {
        this.Dismiss()
      }, 5000);
    }
  }

  Dismiss() {
    clearTimeout(this.hideTimer);
    this.props.HideNotification();
  }
    
  render() {
    const {text, HideNotification} = this.props;
    
    const ClassNames = () => text ? "" : "hidden";
    return (
      <div 
        id="info-toaster" 
        className={ClassNames()}
        onClick={() => {this.Dismiss()}} >
        {text}
      </div>
    )
  }
}