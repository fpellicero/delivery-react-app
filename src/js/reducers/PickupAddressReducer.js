import Types from '../actions/Types';
import AddressReducer from './AddressReducer';

export default function(state = {}, action) {
  if(action.type === Types.RESET_FORM) return {};
  if(action.type !== Types.SET_PICKUP_ADDRESS) return state;

  return AddressReducer(action.value);
}