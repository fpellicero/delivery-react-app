const IsValid = (address) => !address.code;

export default (address) =>  {
  if(!address) return {Filled: false};

  return {
    Filled: true,
    Valid: IsValid(address),
    Value: address
  };
}