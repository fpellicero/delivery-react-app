import {combineReducers} from 'redux';
import PickupAddress from './PickupAddressReducer';
import DropoffAddress from './DropoffAddressReducer';
import Notification from './NotificationReducer';
import RequestPending from './RequestPendingReducer';

export default combineReducers({
  PickupAddress,
  DropoffAddress,
  Notification,
  RequestPending
});