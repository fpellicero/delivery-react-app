import Types from '../actions/Types';

export default function(state = false, action) {
  switch(action.type) {
    case Types.SET_REQUEST_PENDING:
      return !!action.value;
    case Types.RESET_FORM:
      return false;
    default:
      return state;
  }
}