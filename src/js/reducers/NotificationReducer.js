import Types from '../actions/Types';

export default function(state = '', action) {
  switch(action.type) {
    case Types.SHOW_NOTIFICATION:
      return action.value;
    case Types.HIDE_NOTIFICATION:
      return '';
    default:
      return state;
  }
}