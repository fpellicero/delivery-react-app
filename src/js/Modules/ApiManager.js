export function Geocode (address) {
  const data = {
    address: address
  };
  return Post("//localhost:4000/geocode", data);
}

export function CreateJob(PickupAddress, DropoffAddress) {
  const data = {
    pickup: PickupAddress.Value.address,
    dropoff: DropoffAddress.Value.address
  };
  return Post("//localhost:4000/jobs", data);
}

const Post = (endpoint, data) => (
  fetch(endpoint, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  .then((response) => response.json())
);