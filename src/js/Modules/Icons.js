
export default {
  PICKUP_MARKER: "/pickUpMarker.svg",
  PICKUP_EMPTY_FIELD: "/pickUpBadgeBlank.svg",
  PICKUP_ERROR_FIELD: "/pickUpBadgeError.svg",
  PICKUP_OK_FIELD: "/pickUpBadgePresent.svg",

  DROPOFF_MARKER: "/dropOffMarker.svg",
  DROPOFF_EMPTY_FIELD: "/dropOffBadgeBlank.svg",
  DROPOFF_ERROR_FIELD: "/dropOffBadgeError.svg",
  DROPOFF_OK_FIELD: "/dropOffBadgePresent.svg",
}