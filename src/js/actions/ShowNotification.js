import Types from './Types';

export default (text) => (
  {
    type: Types.SHOW_NOTIFICATION,
    value: text
  }
);