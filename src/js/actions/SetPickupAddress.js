import Types from './Types';

export default (address) => (
  {
    type: Types.SET_PICKUP_ADDRESS,
    value: address
  }
);