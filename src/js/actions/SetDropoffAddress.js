import Types from './Types';

export default (address) => (
  {
    type: Types.SET_DROPOFF_ADDRESS,
    value: address
  }
);