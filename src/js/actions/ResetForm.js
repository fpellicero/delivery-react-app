import Types from './Types';

export default () => (
  {
    type: Types.RESET_FORM,
    value: ''
  }
);