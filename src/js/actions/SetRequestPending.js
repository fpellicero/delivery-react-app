import Types from './Types';

export default (value) => (
  {
    type: Types.SET_REQUEST_PENDING,
    value: value
  }
);