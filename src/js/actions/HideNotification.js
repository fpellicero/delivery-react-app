import Types from './Types';

export default () => (
  {
    type: Types.HIDE_NOTIFICATION,
    value: ''
  }
);