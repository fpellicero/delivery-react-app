import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import App from './Containers/App';

const root = document.getElementById('app-root');

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>
  , root);
}

render(App);

if(module.hot) {
  module.hot.accept('./Containers/App', () => {
    render(App);
  });
}