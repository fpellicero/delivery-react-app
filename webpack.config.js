var path = require('path');
const webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

const es2015preset = ['es2015', {'modules': false}];

const babelPlugins = ['transform-runtime', 'transform-object-rest-spread', 'transform-class-properties'];

module.exports = {
    entry: {
        app: ['react-hot-loader/patch', './src/js/index'],
        vendor: ['react', 'react-dom']
    },
    output: {
        publicPath: '/',
        path: path.join(__dirname, 'dist'),
        filename: '[name].bundle.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: babelPlugins,
                        presets: [es2015preset, 'react']
                    }
                },
                
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: babelPlugins,
                        presets: [es2015preset]
                    }
                },
                
            },
            {
                test: /\.scss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    externals: {
        React: 'react',
        ReactDOM: 'react-dom'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({name: "vendor", minChunks: Infinity}),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            chunks: ['vendor', 'app'],
            chunksSortMode: 'manual'
        })
    ],
    devServer: {
        historyApiFallback: false,
        contentBase: [path.join(__dirname, "assets")],
        compress: true,
        port: process.env.PORT || 9000 
    }
}
